﻿using TaskApi.Context;
using TaskApi.ITaskRepository;
using TaskApi.Models;

namespace TaskApi.TaskRepository
{
    public class TaskRepo : ITask
    {
        TaskApiDbContext _context;

        public TaskRepo(TaskApiDbContext context)
        { 
          _context = context;
        }

        public void AddTask(Task1 task)
        {
           _context.Tasks.Add(task);
            _context.SaveChanges();
        }

        public void DeleteTask(int id)
        {
           Task1 task = _context.Tasks.FirstOrDefault( x => x.Id == id);

            if (task != null)
            { 
              _context.Tasks.Remove(task);
                _context.SaveChanges();
            }

        }

        public List<Task1> GetAllTasks()
        {
            return _context.Tasks.ToList();
        }

        public List<SubTask> GetSubTasks(int id)
        {
            return _context.SubTasks.Where(x => x.TaskId == id).ToList();

        }

        public Task1 GetTaskById(int id)
        {
           Task1 task = _context.Tasks.FirstOrDefault( x=> x.Id == id);
            return task;
        }

        public void UpdateTask(int id, Task1 task)
        {
           Task1 task_ = _context.Tasks.FirstOrDefault( x => x.Id == id);

            if (task_ != null)
            {
                task_.TaskName = task.TaskName;
                task_.TaskDescription = task.TaskDescription;
                task_.CreatedOn = task.CreatedOn;
                task_.CreatedBy = task.CreatedBy;
                _context.SaveChanges();
            }
        }
    }
}
