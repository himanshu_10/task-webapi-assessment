﻿using TaskApi.Models;

namespace TaskApi.ITaskRepository
{
    public interface ISubTask
    {

        public void AddSubTask(SubTask sbtsk);
        public List<SubTask> GetSubTasksByTask(int id);

    }
}
