﻿using TaskApi.Models;

namespace TaskApi.ITaskRepository
{
    public interface ITask
    {
        //Declare all following functions only for CRUD for Task & SubTask

        // Add a Task
        // Get all Tasks
        // Get a particular Task
        // Edit some Task
        // Delete some task, in case there is no subtask

        // Add a SubTack for a Task
        // Get all subtasks for a particular Task
        // Here in display Task Name, SubtTask Name,Created By, When
        public List<Task1> GetAllTasks();

        public Task1 GetTaskById(int id);
        public void AddTask(Task1 task);
        public void UpdateTask(int id, Task1 task);
        public void DeleteTask(int id);
        public List<SubTask> GetSubTasks(int id);



    }
}
