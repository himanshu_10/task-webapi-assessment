﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using TaskApi.ITaskRepository;
using TaskApi.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TaskApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        // Add required dependencies here

        ITask _repo;

        public TaskController(ITask repo)
        {
            _repo = repo;  
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetTasks()
        {
            if (_repo.GetAllTasks().ToList().Count == 0)
                return NotFound();
            else

                return Ok(_repo.GetAllTasks());
        }

        [HttpGet("{id}")]

        public IActionResult GetTaskById(int id)
        {
            if (_repo.GetTaskById(id) == null)
                return NotFound("There is no Task");
            else
                return Ok(_repo.GetTaskById(id));
        }

        [HttpPost]
        public IActionResult AddTask(Task1 task)
        {
            _repo.AddTask(task);
            return Created("Created", task);
        }

        [HttpPut("{id}")]
        public IActionResult EditTask(int id, Task1 task)
        {
            Task1 task1 = _repo.GetTaskById(id);
            if (task1 == null)
            {
                return NotFound("There is no Task");
            }
            else
            {
                _repo.UpdateTask(id, task);
                return Ok(task1);
            }

        }
        [HttpDelete("{id}")]

        public IActionResult DeleteTask(int id)
        {
            if (_repo.GetTaskById(id) == null)
            {
                return NotFound("There is no Task");
            }
            else if (_repo.GetSubTasks(id).Count() != 0)
            {
                return BadRequest("Cannot delete Task");
            }
            else
            {
                _repo.DeleteTask(id);
                return Ok("Deleted");
            }


        }

    }
}
