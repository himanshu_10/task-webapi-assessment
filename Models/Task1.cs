﻿namespace TaskApi.Models
{
    public class Task1
    {
       public int Id { get; set; } 
        public string TaskName { get; set; }
        public string TaskDescription { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
    }
}
