﻿namespace TaskApi.Models
{
    public class SubTask
    {// Add properties
        // Id, SubTaskName, Created By, Created On, Description,
        // TaskId

        public int Id { get; set; } 
        public string SubTaskName { get; set; }
        public string SubTaskDescription { get; set; }
        public DateTime CreatedBy { get; set; }
        public int TaskId { get; set; }
        public Task1 ? Task { get; set; }

    }
}
